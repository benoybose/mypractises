function intersect(a1, a2) {
    let i = 0, j = 0;
    let m = a1.length, n = a2.length;
    let result = [];

    while((i < m) && (j < n)) {
        if(a1[i]< a2[j]) {
            i++;
        } else if(a1[i] > a2[j]) {
            j++;
        } else {
            result.push(a1[i]);
            i++;
            j++;
        }
    }
    return result;
}

let practise1 = {};
practise1.intersect = intersect;
module.exports = practise1;